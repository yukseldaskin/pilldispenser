﻿using PillDispenser.Data;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace PillDispenser.Controllers
{
    public class PillsController : ApiController
    {
        // get pills status
        // blue, red, yellow

        // GET api/pills
        public IEnumerable<string> Get()
        {
            DateTime now = DateTime.Now;
            var reception = EventManager.FilteredData(now.AddMinutes(-10), now);

            // empty
            if (string.IsNullOrEmpty(reception))
                return new string[0];

            return ParseReception(reception);
        }

        private IEnumerable<string> ParseReception(string reception)
        {
            var result = new List<string>();
            var tokens = reception.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var token in tokens)
            {
                var part = token.Split(':');
                int count;
                if (!int.TryParse(part[1], out count))
                    throw new ArgumentException("wrong event format");

                for (int i = 0; i < count; i++)
                    result.Add(part[0].ToLower());
            }
            return result;
        }
    }
}