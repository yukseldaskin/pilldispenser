﻿using System;
using PillDispenser.Data;
using System.Web.Http;

namespace PillDispenser.Controllers
{
    public class ReceptionsController : ApiController
    {
        // reception successful
        // pills missed { green / red / etc }

        [HttpPost]
        public void ReportStatus(ReceptionStatus model)
        {
            string message = GetMessage(model);

            var sender = new EmailSender();
            sender.SendEmail("kawaiitest0@gmail.com", message);
        }

        private string GetMessage(ReceptionStatus model)
        {
            string upper = model.Status.ToUpper();
            switch (upper)
            {
                case "SUCCESS":
                    return "Pills was delivered.";
                case "REFILL":
                    return string.Format("Pills of {0} color is missed", model.Color);
                default:
                    throw new ArgumentException();
            }
        }
    }

    public class ReceptionStatus
    {
        public string Status { get; set; }
        public string Color { get; set; }
    }
}