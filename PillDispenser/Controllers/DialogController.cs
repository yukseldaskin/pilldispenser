﻿/*
Copyright © 2005 - 2014 Annpoint, s.r.o.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-------------------------------------------------------------------------

NOTE: Reuse requires the following acknowledgement (see also NOTICE):
This product includes DayPilot (http://www.daypilot.org) developed by Annpoint, s.r.o.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DayPilot.Web.Mvc;
using DayPilot.Web.Mvc.Json;
using PillDispenser.Data;

namespace MvcApplication1.Controllers
{
    [HandleError]
    public class DialogController : Controller
    {

        public ActionResult New(string id)
        {
            return View(new Event
            {
                Start = Convert.ToDateTime(Request.QueryString["start"]),
                End = Convert.ToDateTime(Request.QueryString["end"])
            });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(FormCollection form)
        {
            DateTime start = Convert.ToDateTime(form["Start"]);
            DateTime end = Convert.ToDateTime(form["End"]);
            var recurring = form["Recurring"];
            if (recurring.Contains("true"))
            {
                for (var i = 0; i <= (end-start).Days ;i++ )
                    EventManager.EventCreate(start.AddDays(i), start.AddDays(i).AddMinutes(30), form["amounts"]);

            }
            else
            {
                EventManager.EventCreate(start, end, form["amounts"]);
            }
            return JavaScript(SimpleJsonSerializer.Serialize("OK"));
        }


        public ActionResult Edit(string id)
        {
            var e = EventManager.Get(id) ?? new Event();
            return View(e);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(FormCollection form)
        {
            EventManager.EventEdit(form["Id"], form["Text"]);
            return JavaScript(SimpleJsonSerializer.Serialize("OK"));
        }


    }
}
