﻿function Get-VersionScriptFiles{
   param(
        [string] $folder= $(throw "Please specify a path.")
      )

    $scripts=@()
    $scriptFiles= Get-ChildItem -Path $folder
    foreach($scriptFile in $scriptFiles){
       #Write-Host $scriptFile
       if($scriptFile -match "(\d+\.\d+\.\d+\.\d+)")
       {
        $version=[Version]$Matches[0]
        $scripts+= $version
       }
    }
    $scripts= $scripts|Sort-Object
    $scripts
}

function Invoke-SQL {
    param(
        [string] $dataSource = ".\SQLEXPRESS",
        [string] $database = "MasterData",
        [string] $sqlCommand = $(throw "Please specify a query.")
      )

    $connectionString = "Data Source=$dataSource; " +
            "Integrated Security=SSPI; " +
            "Initial Catalog=$database"

    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand($sqlCommand,$connection)
    $connection.Open()

    $adapter = New-Object System.Data.sqlclient.sqlDataAdapter $command
    $dataset = New-Object System.Data.DataSet
    $adapter.Fill($dataSet) | Out-Null

    $connection.Close()
    $dataSet.Tables

}
function Get-DbVersion{
   param(
        [string] $server= $(throw "Please specify a server."),
        [string] $database= $(throw "Please specify a database.")

      )
    $dbVersion=[Version]"0.0.0.0"
    try{
    $dbversionTable=Invoke-SQL $server $database "Select [Version] From [Info]"
    $dbVersion=[Version]$dbversionTable[0].Version
    }
    catch{}

    $dbVersion
}

function Set-DbVersion{
   param(
        [string] $server= $(throw "Please specify a server."),
        [string] $database= $(throw "Please specify a database."),
        [string] $version

      )
      Invoke-SQL $server $database "IF(NOT EXISTS(Select * From Info)) INSERT INTO [Info](Version) Values('0.0.0.0')"      
      Invoke-SQL $server $database "UPDATE [Info] Set [Version]='$version'"
    
}



