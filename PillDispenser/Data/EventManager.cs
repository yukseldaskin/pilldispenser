﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace PillDispenser.Data
{
    /// <summary>
    /// Summary description for EventManager
    /// </summary>
    public static class EventManager
    {
        public static DataTable Data;

        public static void FillData()
        {
            Data = GenerateData();
        }
        public static string FilteredData(DateTime start, DateTime end)
        {
            string where = string.Format("NOT (([end] <= '{0:s}') OR ([start] >= '{1:s}'))", start, end);
            DataRow[] rows = Data.Select(where);
            DataTable filtered = Data.Clone();

            foreach (DataRow r in rows)
            {
                filtered.ImportRow(r);
            }

            var reception = filtered.Select().FirstOrDefault();
            if (reception == null)
                return null;

            return (string)reception["amounts"];
        }


        private static DataTable GenerateData()
        {
            var table = new DataTable();
            using (var da = new SqlDataAdapter("SELECT * FROM schedule", ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                da.Fill(table);
            }
            table.PrimaryKey = new DataColumn[] { table.Columns[0] };
            return table;
        }

        public static void EventEdit(string id, string pills)
        {
            DataRow dr = Data.Rows.Find(id);
            if (dr != null)
            {
                dr["amounts"] = pills;
                Data.AcceptChanges();
            }
        }

        public static void EventMove(string id, DateTime start, DateTime end)
        {
            DataRow dr = Data.Rows.Find(id);
            if (dr != null)
            {
                dr["start"] = start;
                dr["end"] = end;
                Data.AcceptChanges();
            }
        }

        public static Event Get(string id)
        {
            DataRow dr = Data.Rows.Find(id);
            if (dr == null)
            {
                //return new Event();
                return null;
            }
            return new Event()
                       {
                           Id = dr["id"].ToString(),
                           PillsString = dr["amounts"].ToString()
                       };
        }

        private static int _counter = 100;
        internal static void EventCreate(DateTime start, DateTime end, string pills)
        {
            DataRow dr = Data.NewRow();

            dr["id"] = CreateInDB(start, end, pills);
            dr["start"] = start;
            dr["end"] = end;
            dr["amounts"] = pills;

            Data.Rows.Add(dr);
            Data.AcceptChanges();
        }

        public static int CreateInDB(DateTime start, DateTime end, string amounts)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("INSERT INTO Schedule([start],[end],[amounts]) output INSERTED.id VALUES(@start,@end,@amounts)", con))
                {
                    cmd.Parameters.AddWithValue("@start", start);
                    cmd.Parameters.AddWithValue("@end", end);
                    cmd.Parameters.AddWithValue("@amounts", amounts);
                    con.Open();

                    int modified = (int)cmd.ExecuteScalar();

                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();

                    return modified;
                }
            }
        }



        public static void EventDelete(string id)
        {
            DataRow dr = Data.Rows.Find(id);
            if (dr != null)
            {
                Data.Rows.Remove(dr);
                Data.AcceptChanges();
            }
        }
    }
}