using System;
using System.Collections.Generic;
using System.Text;

namespace PillDispenser.Data
{
    public class Event
    {
        public string Id { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public Dictionary<string, int> Pills
        {
            get { return _pills; }
            set { _pills = value; }
        }

        private Dictionary<string, int> _pills;

        public string PillsString
        {
            get
            {
                var result = new StringBuilder();
                foreach (KeyValuePair<string, int> pill in _pills)
                {
                    result.Append(pill.Key);
                    result.Append(":");
                    result.Append(pill.Value);
                    result.Append(";");
                }
                return result.ToString();
            }
            set
            {
                _pills = new Dictionary<string, int>();
                var pillCombos = value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var pillCombo in pillCombos)
                {
                    var pillComboGroups = pillCombo.Split(new string[] { ":" }, StringSplitOptions.None);
                    _pills.Add(pillComboGroups[0], Int32.Parse(pillComboGroups[1]));
                }
            }
        }
    }



}