﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;

namespace PillDispenser.Data
{
    public class EmailSender
    {
        const string fromEmail = "PillDispenser1@gmail.com";
        const string fromPassword = "SuperPassword";
        const string subject = "Pill Dispenser Report";
        
        public void SendEmail(string recipient, string msg)
        {
            var fromAddress = new MailAddress(fromEmail, "Pills Dispenser");
            var toAddress = new MailAddress(recipient, "Patient Zero");

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };

            using (var mail = new MailMessage(fromAddress, toAddress) { Subject = subject, Body = msg })
            {
                smtp.Send(mail);
            }
        }
    }
}