﻿using System;
using System.Data;
using DayPilot.Web.Mvc;
using DayPilot.Web.Mvc.Enums;
using DayPilot.Web.Mvc.Events.Calendar;
using PillDispenser.Data;

namespace PillDispenser
{
    public class Dpc : DayPilotCalendar
    {
        protected override void OnTimeRangeSelected(TimeRangeSelectedArgs e)
        {
            string name = (string)e.Data["name"];
            if (String.IsNullOrEmpty(name))
            {
                name = "(default)";
            }
            EventManager.EventCreate(e.Start, e.End, name);
            Update();
        }

        protected override void OnEventMove(DayPilot.Web.Mvc.Events.Calendar.EventMoveArgs e)
        {
            if (EventManager.Get(e.Id) != null)
            {
                EventManager.EventMove(e.Id, e.NewStart, e.NewEnd);
            }

            Update();
        }

        protected override void OnEventClick(EventClickArgs e)
        {

        }

        protected override void OnEventResize(DayPilot.Web.Mvc.Events.Calendar.EventResizeArgs e)
        {
            EventManager.EventMove(e.Id, e.NewStart, e.NewEnd);
            Update();
        }

        protected override void OnCommand(CommandArgs e)
        {
            switch (e.Command)
            {
                case "navigate":
                    StartDate = (DateTime)e.Data["start"];
                    Update(CallBackUpdateType.Full);
                    break;

                case "refresh":
                    Update();
                    break;

                case "previous":
                    StartDate = StartDate.AddDays(-7);
                    Update(CallBackUpdateType.Full);
                    break;

                case "next":
                    StartDate = StartDate.AddDays(7);
                    Update(CallBackUpdateType.Full);
                    break;

                case "today":
                    StartDate = DateTime.Today;
                    Update(CallBackUpdateType.Full);
                    break;

            }
        }

        protected override void OnInit(InitArgs initArgs)
        {
            Update(CallBackUpdateType.Full);
        }

        protected override void OnFinish()
        {
            // only load the data if an update was requested by an Update() call
            if (UpdateType == CallBackUpdateType.None)
            {
                return;
            }

            // this select is a really bad example, no where clause
            Events = EventManager.Data.AsEnumerable();


            DataStartField = "start";
            DataEndField = "end";
            DataTextField = "amounts";
            DataIdField = "id";
        }

    }
}
