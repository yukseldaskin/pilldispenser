﻿IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Schedule'))
BEGIN

	CREATE TABLE [dbo].[Schedule] (
    [id]   INT           IDENTITY (1, 1) NOT NULL,
    [start] Datetime2 NULL,
    [end] Datetime2 NULL,
	[amounts] nvarchar(64) NULL,
    CONSTRAINT [PK_Table_Schedule] PRIMARY KEY CLUSTERED ([id] ASC)
);

END

GO

IF(NOT EXISTS(SELECT TOP 1 * FROM [dbo].[Schedule] WHERE amounts='green:1'))
BEGIN
INSERT INTO [dbo].[Schedule]
Values(Dateadd(minute,1, GETDATE()) , Dateadd(minute,30, GETDATE()),'green:1')
END
GO


IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Info'))
BEGIN

	CREATE TABLE [dbo].[Info] (
    [id]   INT           IDENTITY (1, 1) NOT NULL,
    [Version] nvarchar(16),
    CONSTRAINT [PK_Table_Info] PRIMARY KEY CLUSTERED ([id] ASC)
);

END

GO
