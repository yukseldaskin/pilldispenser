﻿
	CREATE TABLE [dbo].[Schedule] (
    [id]   INT           IDENTITY (1, 1) NOT NULL,
    [start] Datetime2 NULL,
    [end] Datetime2 NULL,
	[amounts] nvarchar(64) NULL,
    CONSTRAINT [PK_Table_Schedule] PRIMARY KEY CLUSTERED ([id] ASC)
);

GO



INSERT INTO [dbo].[Schedule]
Values(Dateadd(minute,1, GETDATE()) , Dateadd(minute,30, GETDATE()),'green:1')

GO

INSERT INTO [dbo].[Schedule]
Values(Dateadd(minute,1, GETDATE()) , Dateadd(minute,90, GETDATE()),'blue:2')
GO
