﻿IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Schedule'))
BEGIN

	CREATE TABLE [dbo].[Schedule] (
    [id]   INT           IDENTITY (1, 1) NOT NULL,
    [start] Datetime2 NULL,
    [end] Datetime2 NULL,
	[amounts] nvarchar(64) NULL,
    CONSTRAINT [PK_Table_Schedule] PRIMARY KEY CLUSTERED ([id] ASC)
);

END

GO

IF(NOT EXISTS(SELECT TOP 1 * FROM [dbo].[Schedule] WHERE amounts='green:1'))
BEGIN
INSERT INTO [dbo].[Schedule]
Values(Dateadd(minute,1, GETDATE()) , Dateadd(minute,30, GETDATE()),'green:1')
END
GO

IF(NOT EXISTS(SELECT TOP 1 * FROM [dbo].[Schedule] WHERE amounts='blue:2'))
BEGIN
INSERT INTO [dbo].[Schedule]
Values(Dateadd(minute,1, GETDATE()) , Dateadd(minute,90, GETDATE()),'blue:2')
END
GO

